**Note:** 
This is an example repo to showcase (and document) the configuration of the GitLab Kubernetes Agent. For more information see https://gitlab.com/gitlab-org/gitlab/-/issues/216569

# Configuration of the GitLab Kubernetes Agent (GKA)

To install GKA into your cluster, you should get a token. For step by step instructions on how to install GKE read &lt;docs link comes here>. As part of those steps you'll be directed to this repo to describe the configuration of the agent.

Initially this repo contains two sets of configurations:

- GKA configuration file: [./config](./config)
- optional RBAC configuration to run the agent with: [rbac.yaml](./rbac.yaml)

## GKA configuration

The configuration file will be read by the agent when it registers itself with GitLab, and periodically afterwards when the file changes.

## RBAC configuration

The initial configuration file provided in this repo gives `cluster-admin` rights to the agent. This allows the agent to use all the features and provides the best integrated experience within GitLab. You are free to apply a custom RBAC for the agent, but expect that certain functions might not work in this case as expected. These are documented in more detail in [rbac.yaml](./rbac.yaml)
